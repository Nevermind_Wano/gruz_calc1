import '../css/calc_styles.scss';

let step = 1;

/**
 * Шаг 1
 */
import step1 from './step1';
import step2 from './step2';
step1();
step2();

$('form.calc').css('overflow-x', 'hidden');

$('.btnDalee').on('click', function (e) {
    e.preventDefault();
    $('.step' + step).css('left', '-200%');
    let nextStep = step + 1;
    step++;
    $('.step' + nextStep).css('left', '50%');


});


$('.btnPrev').on('click', function (e) {
    e.preventDefault();
    $('.step' + step).css('left', '200%');
    let nextStep = step - 1;
    step--;
    $('.step' + nextStep).css('left', '50%');
});


