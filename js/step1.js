const step1 = () => {
    /**
     * Инициализация переменных
     */
    window._distance = 0;
        // Подсказки
    let suggestView,
        // Точки на карте
        points = [],
        // Объект карты
        myMap = null,
        // Счётчик точек маршрута
        countAddresses = 2,
        // Адреса
        addresses = [],
        // Шаблон input'а
        template = `
            <div class="group">
                <input type="text" id="address-%idAddr%" size="50" value="%value%" required>
                <label>%label%</label>
                %del%
            </div>
          `,
        // Метки для input'ов адресов
        labels = [
            'Откуда',
            'Куда',
            'Куда ещё',
            'Конечный адрес'
        ];

    // Маршрут по умолчанию
    const defaultAddrs = ["Москва", "Тверь"];

    ymaps.ready(init);

    function init() {
        $(document).ready(function () {
            setListeners();
        })
    }

    /**
     * Настройка параметров карты
     */
    function setupMap() {

        // Обнуляем переменную и удаляем тэги Я.Карт
        myMap = null;
        $(".map #map ymaps").remove();


        myMap = new ymaps.Map('map', {
            center: [56.399625, 36.71120],
            zoom: 7,
        }, {
            buttonMaxWidth: 300
        });

        setupRouter();
    }

    /**
     * Настройка маршрутизации
     */
    function setupRouter() {
        points = fillArr();
        let referencePoints = [];


        if (points.length === 1)
            referencePoints = [points[0], "Москва"];
        else if (points.length < 1)
            referencePoints = defaultAddrs;
        else
            referencePoints = points;

        // console.log("points = " + points);

        ymaps.route(
            referencePoints,
            {
                mapStateAutoApply: true
            })

            .then(function (route) {
                fillAddresses(route);

                myMap.geoObjects.add(route);

                if (points.length > 1)
                    setDistance(route.getLength());

                // При любом изменении маршрута заполняем массив адресов и
                // заполняем тэг с классом distance
                route.editor.events.add(["start", "routeupdate"], function (e) {
                    fillAddresses(route);
                    setDistance(route.getLength());
                });

                // Включаем режим редактирования только на удаление точек
                route.editor.start({addWayPoints: false, removeWayPoints: true});

                /**
                 * Обработка событий карты и редактора
                 */

                // При клике на любое место на карте включается режим редактирования с возможностью добавлять точки
                myMap.events.add('click', function (e) {
                    if (route.getWayPoints().getLength() < 4) {
                        route.editor.start({addWayPoints: true, removeWayPoints: true});
                        countAddresses = route.getWayPoints().getLength();
                    }
                });

                // После того как точка добавлена ил удалена - отключаем режим добавления точек
                route.editor.events.add(["waypointadd", "waypointremove"], function () {
                    route.editor.start({
                        addWayPoints: false,
                        removeWayPoints: true,
                        editViaPoints: false,
                        addViaPoints: false
                    });
                    countAddresses = route.getWayPoints().getLength();
                });


            }, function (error) {
                alert("Возникла ошибка: " + error.message);
            });
    }

    /**
     * Заполняем массив точек данными из input'ов адрессов
     *
     * @returns {Array}
     */
    function fillArr() {
        points = [];
        $(".addreses :input").each(function (key, value) {
            if ($(this).val() != "") {
                points[key] = $(this).val();
            }
        });
        return points;
    }

    /**
     * Заполняем массив адресов
     *
     * @param route
     * @returns {Promise<void>}
     */
    async function fillAddresses(route) {
        points = route.requestPoints;

        for (let key in points) {
            let myReverseGeocoder = ymaps.geocode(points[key]);
            await myReverseGeocoder.then(function (res) {
                addresses[key] = res.geoObjects.get(0).properties.get('text');
            });
        }
    }

    async function setDistance(pathLenght) {
        // console.log(pathLenght);
        window._distance = Math.floor(pathLenght / 1000);
        $(".distance").empty()
            .append(`Общее расстояние..................... ${window._distance} км.`);
    }

    /**
     * Регистрируем слушателей событий элементов dom
     */
    function setListeners() {

        /**
         * При фокусе на input адресов включаем подсказки
         */
        $('.addreses').on('focus', 'input', function () {
            var id = $(this).attr("id");
            $(".addreses .group ymaps").remove();
            suggestView = new ymaps.SuggestView(id); // подсказки
            suggestView.events.add('select', function (event) {
                //console.log(event.get('item').value);

                setupMap();
            });
        });

        $('.datetime').on('focus', 'input', function () {
            $('.timeFeed input[type=radio]').prop("checked", false);
        });

        $('.timeFeed input[type=radio]').change(function() {
            $('.datetime input').val("");
        });

        /**
         * При клике на кнопку "сохранить" в модалке карты
         * перерисовываем все инпуты адресов с заполнением
         * их нужными значениями
         */
        $('a.saveMap').on('click', function (event) {
            event.preventDefault();

            $(".addreses").html(function () {
                // let output = '<div class="addreses">';
                let output = '';

                $(points).each(function (index, value) {
                    let address = '';
                    let input = template.replace(/%idAddr%/, index)
                        .replace(/%label%/, labels[index])
                        .replace(/%value%/, addresses[index]);
                    if (index > 1)
                        input = input.replace(/%del%/, `<span>Удалить</span>`);
                    else
                        input = input.replace(/%del%/, '');

                    output += input;
                });

                return output;

            });

            // Закрываем модалку
            $('div.modal').css('display', 'none');
        });

        /**
         * Добавляем новое поле при клике на "Добавить адрес"
         */
        $(".addAddress").click(function () {

            if (countAddresses >= 4)
                return;
            let input = template.replace(/%idAddr%/, countAddresses)
                .replace(/%label%/, labels[countAddresses])
                .replace(/%value%/, '')
                .replace(/%del%/, `<span>Удалить</span>`);

            $(".addreses").append(input);
            countAddresses++;
        });

        /**
         * Удаляем поле при клике на "Удалить"
         */
        $('.addreses').on('click', 'span', function () {
            $(this).parent('div').remove();
            countAddresses--;
        });

        /**
         * Открываем модалку с картой при клике на "Показать карту"
         */
        $('.showMap').on('click', function () {
            setupMap();
            $('div.modal').css('display', 'block');
        });

        /**
         * Закрыть модалку при клике на крестик
         */
        $('.close-map').on('click', function () {
            $('div.modal').css('display', 'none');
        });

        /**
         * Закрыть модалку при клике на любое место вне модалки
         */
        let modal = document.querySelector('.modal');
        window.onclick = function (event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        };
    }

};

export default step1;
